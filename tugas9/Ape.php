<?php

require_once('animal.php');

class Ape extends Animal
{
    public $name;
    public $legs = 4;
    public $cold_blooded = 0;
    public $jump;


    public function yell()
    {
        echo " Jump : Auooo";
    }
}
