<?php
class Animal
{
    public $name;
    public $legs = 4;
    public $cold_blooded = 0;

    public function __construct($string)
    {
        $this->name = $string;
    }
}
